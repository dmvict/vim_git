" Author: DmVict ( dm.vict.kr@gmail.com )
" License: GNU GPL v3 ( http://www.gnu.org/copyleft/gpl.html )

let s:Buffer = g:vim_git#tools#Buffer#
let s:System = vim_lib#sys#System#.new()
let s:Content = vim_lib#sys#Content#.new()
let s:Publisher = vim_lib#sys#Publisher#.new()
let s:BufferStack = vim_lib#view#BufferStack#
let s:DiffTools = g:vim_git#tools#DiffTools#.new()

"//

let s:screen = s:Buffer.new( '#Git-status#' )
call s:screen.au( 'CursorHold', 'redraw' )
call s:screen.option( 'filetype', 'git-status' )
call s:screen.option( 'spell', '0' )
call s:screen.option( 'number', '0' )

"//

""
" Method render() fills buffer `Status` by data.
"
" @returns { List } - Returns data for buffer `Status`.
""

function! s:screen.render()
    let l:result = [ '  " Status (Press ? for help) "', '' ] + s:statusParse( vim_git#run( 'status -s -b' ) )
    if g:vim_git#statusWithStash
        let l:stashesLength = s:repositoryCountStashes()
        if l:stashesLength != 0
            let l:lastline = len( l:result )
            call insert( l:result, '  Repository has ' . l:stashesLength . ' stash(es)', l:lastline > 4 ? 4 : l:lastline )
        endif
    endif
    return l:result
endfunction

"//

""
" Method title() makes string with branch name and remote branch name.
"
" @param { List } dst - Destination container, the concatenated string appends to it.
" @param { String } prefix - Prefix for string {-line-}.
" @param { String } line - Line with path to file, the method cut first 3 symbols of it.
"
" @returns {} - Returns not a value, appends formatted string to destination container.
""

function! s:title( dst, prefix, line )
    let l:splits = split( a:line, '\.\.\.' )
    let l:branch =  l:splits[ 0 ][ 3: ]
    if len( l:splits ) >= 2
        call add( a:dst, a:prefix . l:branch )
        call add( a:dst, "  Remote branch::" . l:splits[ 1 ] )
    else
        let l:branch = split( l:branch )
        let l:length = len( l:branch )
        if l:length > 1
            call add( a:dst, a:prefix . l:branch[ l:length - 1 ] . "\n  " . join( l:branch[ 0:2 ], ' ' ) )
        else
            call add( a:dst, a:prefix . l:branch[ 0 ] )
        endif
    endif
endfunction

"//

""
" Method appendLine() makes string with path in argument {-line-} and prefix" in argument {-prefix-}
" and appends it to destination container.
"
" @param { List } dst - Destination container, the concatenated string appends to it.
" @param { String } prefix - Prefix for string {-line-}.
" @param { String } line - Line with path to file, the method cut first 3 symbols of it.
"
" @returns {} - Returns not a value, appends formatted string to destination container.
""

function! s:appendLine( dst, prefix, line )
    call add( a:dst, a:prefix . a:line[ 3: ] )
endfunction

"//

""
" Private method statusParse() parses data of command `git status -s -b`.
"
" @param { String } output - Output of the command to parse.
"
" @returns { List } - Returns a List with formatted strings for buffer `Status`.
""

function! s:statusParse( output )

    let l:indexed = []
    let l:unindexed = []
    let l:unmerged = []

    let l:added = '  added : '
    let l:copied = '  copied : '
    let l:deleted = '  deleted : '
    let l:modified = '  modified : '
    let l:renamed = '  renamed : '
    let l:unmerged_ = '  unmerged : '

    let l:hashMap =
    \{
    \  '##' : { 'callback' : function( 's:title' ),      'buffer' : [],          'prefix' : '  Branch::' },
    \  'A ' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:indexed,   'prefix' : l:added },
    \  'C ' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:indexed,   'prefix' : l:copied },
    \  'D ' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:indexed,   'prefix' : l:deleted },
    \  'M ' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:indexed,   'prefix' : l:modified },
    \  'T ' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:indexed,   'prefix' : l:modified },
    \  'R ' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:indexed,   'prefix' : l:renamed },
    \  'AM' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unindexed, 'prefix' : l:added },
    \  'AD' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unindexed, 'prefix' : l:added },
    \  'CM' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unindexed, 'prefix' : l:copied },
    \  'CD' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unindexed, 'prefix' : l:copied },
    \  'MM' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unindexed, 'prefix' : l:modified },
    \  'MD' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unindexed, 'prefix' : l:modified },
    \  'RM' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unindexed, 'prefix' : l:renamed },
    \  'RD' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unindexed, 'prefix' : l:renamed },
    \  ' A' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unindexed, 'prefix' : l:added },
    \  ' C' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unindexed, 'prefix' : l:copied },
    \  ' D' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unindexed, 'prefix' : l:deleted },
    \  ' M' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unindexed, 'prefix' : l:modified },
    \  ' T' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unindexed, 'prefix' : l:modified },
    \  ' R' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unindexed, 'prefix' : l:renamed },
    \  'AA' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unmerged,  'prefix' : l:unmerged_ },
    \  'AU' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unmerged,  'prefix' : l:unmerged_ },
    \  'DD' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unmerged,  'prefix' : l:unmerged_ },
    \  'DU' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unmerged,  'prefix' : l:unmerged_ },
    \  'UA' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unmerged,  'prefix' : l:unmerged_ },
    \  'UD' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unmerged,  'prefix' : l:unmerged_ },
    \  'UU' : { 'callback' : function( 's:appendLine' ), 'buffer' : l:unmerged,  'prefix' : l:unmerged_ },
    \  '??' : { 'callback' : function( 's:appendLine' ), 'buffer' : [],          'prefix' : '  untracked : ' },
    \}

    let l:changedIndexed = { 'AM' : 1, 'AD' : 1, 'CM' : 1, 'CD' : 1, 'MM' : 1, 'MD' : 1, 'RM' : 1, 'RD' : 1 }

    let l:list = split( a:output, "\n" )
    for line in l:list
        let l:key = line[ 0:1 ]
        call l:hashMap[ l:key ].callback( l:hashMap[ l:key ].buffer, l:hashMap[ l:key ].prefix, line )
        if has_key( l:changedIndexed, l:key ) && l:changedIndexed[ l:key ] == 1
            call add( l:indexed, l:hashMap[ l:key ].prefix . line[ 3: ] )
        endif
    endfor

    let l:result = l:hashMap[ '##' ].buffer
    if len( l:indexed ) >= 1
        let l:result += [ '', 'Added to index :', '' ] + l:indexed
    endif
    if len( l:unindexed ) >= 1
        let l:result += [ '', 'Unstaged :', '' ] + l:unindexed
    endif
    if len( l:unmerged ) >= 1
        let l:result += [ '', 'Unmerged :', '' ] + l:unmerged
    endif
    if len( l:hashMap[ '??' ].buffer ) >= 1
        let l:result += [ '', 'Untracked :', '' ] + l:hashMap[ '??' ].buffer
    endif

    return l:result
endfunction

"//

""
" Private method repositoryCountStashes() count quantity of stashes in current repository.
"
" @returns { Number } - Returns integer with quantity of stashes in repository.
""

function! s:repositoryCountStashes()
    let l:stashes = vim_git#run( 'stash list' )
    if l:stashes == ''
        return 0
    endif
    return len( split( l:stashes, "\n" ) )
endfunction

"//

call s:screen.map( 'n', 'a',  'add' )
call s:screen.map( 'n', 'A',  'addAll' )
call s:screen.map( 'n', 'dd', 'delete' )
call s:screen.map( 'n', 'da', 'deleteAll' )
call s:screen.map( 'n', 'u',  'reset' )
call s:screen.map( 'n', 'U',  'resetAll' )
call s:screen.map( 'n', 's',  'stash' )
call s:screen.map( 'n', 'sa', 'stashAll' )
call s:screen.map( 'n', 'd',  'diff' )
call s:screen.map( 'n', 'D',  'vimdiff' )
call s:screen.map( 'n', 'cc', 'commit' )
call s:screen.map( 'n', 'ca', 'commitAll' )
call s:screen.map( 'n', 'pf', 'pushForce' )
if( exists( 'g:vim_git#useAsyncMethods' ) )
    call s:screen.map( 'n', 'P',  'pullAsync' )
    call s:screen.map( 'n', 'p',  'pushAsync' )
else
    call s:screen.map( 'n', 'P',  'pull' )
    call s:screen.map( 'n', 'p',  'push' )
endif

"//

""
" Method add() adds file in line under cursor to the index.
"
" @returns {} - Returns not a value, adds file to the index.
""

function! s:screen.add()
    let l:line = line( '$' )
    call s:Content.pos({ 'c' : col( '$' ) })
    call vim_git#addFile( expand( '<cfile>' ) )
    call self.redraw()
    let l:deltaHeight = line( '$' ) - l:line
    if l:deltaHeight > 0
        call s:Content.pos({ 'l' : line( '.' ) + l:deltaHeight })
    endif
endfunction

"//

""
" Method addAll() adds all unstaged files to the index.
"
" @returns {} - Returns not a value, adds files to the index.
""

function! s:screen.addAll()
    call vim_git#addAll()
    call self.redraw()
endfunction

"//

""
" Method delete() deletes single file in line under cursor from the index.
"
" @returns {} - Returns not a value, deletes file from the index.
""

function! s:screen.delete()
    call s:Content.pos({ 'c' : col( '$' ) })
    call vim_git#resetFile( expand( '<cfile>' ) )
    call self.redraw()
endfunction

"//

""
" Method delete() deletes all files from the index.
"
" @returns {} - Returns not a value, deletes file from the index.
""

function! s:screen.deleteAll()
    call vim_git#resetIndex()
    call self.redraw()
endfunction

"//

""
" Method reset() resets changes in file in line under cursor. If file is
" untracked or untracked ignored, then method deletes the file.
"
" @returns {} - Returns not a value, undo changes in file.
""

function! s:screen.reset()
    call s:Content.pos({ 'c' : col( '$' ) })
    if s:System.confirm( 'Do you want to reset/delete file "' . expand( '<cfile>' ) . '"?' )
        call vim_git#undoFile( expand( '<cfile>' ) )
        call self.redraw()
    endif
endfunction

"//

""
" Method resetAll() resets changes in all files. Untracked files deletes by
" default. To delete untracked ignored files or only tracked use options of
" method.
"
" @returns {} - Returns not a value, undo changes in all files.
""

function! s:screen.resetAll()
    call s:System.echo( 'If you want to reset files, select the variant please:', 'ModeMsg' )
    call s:System.echo( '0 - reset only indexed files.' )
    call s:System.echo( '1 - reset indexed and untracked files.' )
    call s:System.echo( '2 - reset indexed, untracked and untracked ignored files.' )
    call s:System.echo( 'y/Y - reset indexed and untracked files.' )
    call s:System.echo( 'n/N/<CR> - reset no files.' )
    let l:answer = s:System.read( '' )
    if l:answer != '' && l:answer != "n" && l:answer != "N"
        if l:answer == 2
            call vim_git#undoChanges( 'untrackedIgnored' )
        elseif l:answer == 1 || l:answer == "y" || l:answer == "Y"
            call vim_git#undoChanges( 'untracked' )
        elseif l:answer == 0
            call vim_git#undoChanges()
        endif
    endif
    call self.redraw()
endfunction

"//

""
" Method stash() stashes single file in line under cursor.
"
" @returns {} - Returns not a value, stashes file.
""

function! s:screen.stash()
    let l:line = line( '$' )
    call s:Content.pos({ 'c' : col( '$' ) })
    call vim_git#stashFile( expand( '<cfile>' ) )
    call self.redraw()
    let l:deltaHeight = line( '$' ) - l:line + 1
    if l:deltaHeight > 0
        call s:Content.pos({ 'l' : line( '.' ) + l:deltaHeight })
    endif
endfunction

"//

""
" Method stashAll() stashes all files in dirty git repository.
"
" @returns {} - Returns not a value, stashes all files.
""

function! s:screen.stashAll()
    call s:Content.pos({ 'c' : col( '$' ) })
    call vim_git#stashAll()
    call self.redraw()
endfunction

"//

""
" Method diff() shows changes in file in line under cursor.
"
" @returns {} - Returns not a value, shows changes in file.
""

function! s:screen.diff()
    call s:Content.pos({ 'c' : col( '$' ) })
    call s:DiffTools.diff( self, "vim_git#run('diff HEAD -- " . expand( '<cfile>' ) . "')" )
endfunction

"//

""
" Method vimdiff() is intended to show changes in file in line under cursor using vimdiff tools.
"
" @returns {} - Returns not a value, shows changes in file.
""

function! s:screen.vimdiff()
    call s:Content.pos({ 'c' : col( '$' ) })
    let l:file = expand( '<cfile>' )
    call s:DiffTools.vimdiff( self, vim_git#run( 'cat-file -p HEAD:' . l:file ), readfile( l:file ) )
endfunction

"//

""
" Method commit() opens commit window buffer. The method emits event `UpdateBufferData` on saved buffer.
"
" @returns {} - Returns not a value, opens buffer to write commit message.
""

function! s:screen.commit()
    call vim_git#commit()
    autocmd BufWritePost <buffer> call s:Publisher.fire( 'UpdateBufferData' )
endfunction

"//

""
" Method commitAll() adds all files to index and opens commit window buffer. The method emits event
" `UpdateBufferData` on saved buffer.
"
" @returns {} - Returns not a value, adds non indexed files and opens buffer to write commit message.
""

function! s:screen.commitAll()
    call vim_git#commitAll()
    autocmd BufWritePost <buffer> call s:Publisher.fire( 'UpdateBufferData' )
endfunction

"//

""
" Method push() pushes commits to server.
"
" @returns {} - Returns not a value, pushes commits to server.
""

function! s:screen.push()
    call vim_git#pushCurrent()
    call self.redraw()
endfunction

"//

""
" Method pushForce() pushes commits to server, and overwrite commits history
" if it has conflicts.
"
" @returns {} - Returns not a value, pushes commits to server.
""

function! s:screen.pushForce()
    call vim_git#pushCurrent( 1 )
    call self.redraw()
endfunction

"//

""
" Method pushAsync() pushes commits to server asynchronously and block no editor.
"
" @returns {} - Returns not a value, pushes commits to server and write data to buffer `Status`.
""

function! s:asyncRedraw( channel, ... )
    call timer_start( 1000, function( 's:_asyncRedraw' ) )
endfunction

function! s:_asyncRedraw( ... )
    call s:screen.redraw()
endfunction

function! s:printData( job_id, data, ... )
    echo join( a:data, "\n" )
    call s:screen.redraw()
endfunction

function! s:screen.pushAsync( ... )
    if has( 'nvim' )
        call vim_git#runAsync( 'push', { 'stdout_buffered' : 1, 'stderr_buffered' : 1, 'on_exit' : function( 's:asyncRedraw' ), 'on_stdout' : function( 's:printData' ), 'on_stderr' : function( 's:printData' ) } )
    else
        call vim_git#runAsync( 'push', { 'err_io' : 'buffer', 'err_buf' : self.getNum(), 'close_cb' : function( 's:asyncRedraw' ) } )
    endif
endfunction

"//

""
" Method pull() pulls all commites and tags from all remote server.
"
" @returns {} - Returns not a value, pulls changes from remote servers.
""

function! s:screen.pull()
    call vim_git#pullAll()
    call s:screen.redraw()
endfunction

"//

function! s:screen.pullAsync( ... )
    if has( 'nvim' )
        call vim_git#runAsync( 'pull --all', { 'stdout_buffered' : 1, 'stderr_buffered' : 1, 'on_exit' : function( 's:asyncRedraw' ), 'on_stdout' : function( 's:printData' ), 'on_stderr' : function( 's:printData' ) } )
    else
        call vim_git#runAsync( 'pull --all', { 'out_io' : 'buffer', 'out_buf' : self.getNum(), 'close_cb' : function( 's:asyncRedraw' ) } )
    endif
endfunction

"//

""
" The List `help` is intended to show key mapping for `Status` buffer by using
" method `showHelp` of the instance.
""

let s:screen.help =
\[
\    '" Manual "',
\    '',
\    '" a - add file under cursor to index',
\    '" A - add all files to index',
\    '" dd - delete file under cursor from index',
\    '" da - delete all files from index',
\    '" u - reset/delete file under cursor',
\    '" U - reset/delete all uncommited/untracked files',
\    '" s - stash file under cursor',
\    '" sa - stash all files',
\    '" d - show changes in the file under cursor',
\    '" D - show changes in the file under cursor with Vim',
\    '" cc - commit files in index',
\    '" ca - commit all files',
\    '" p - push commit',
\    '" pf - push commit force',
\    '" P - pull commits history from remote',
\    '" q - close window',
\    ''
\]

"//

let s:bufStack = s:BufferStack.new()
call s:bufStack.push( s:screen )
call s:screen.ignoreMap( 'n', '<C-y>' )
let vim_git#Status# = s:bufStack
