let s:Object = g:vim_lib#base#Object#
let s:Buffer = g:vim_git#tools#Buffer#

"//

""
" The utilitarian class `vim_git#tools#DiffTools#` is intended to provide common interface
" for getting diffs for different buffers.
""

let s:Class = s:Object.expand()

""
" The constructor new() returns single instance of class.
"
" @returns { Object } - Returns vim_lib#sys#Buffer# destination buffer.
""

function! s:Class.new()
    return self.singleton
endfunction

"//

""
" Method diff() is intended to create buffer with diffs for parent buffer.
"
" @returns {} - Returns not a value, creates and activate diff buffer.
""

function s:Class.diff( parentBuffer, render )
    let l:buffer = s:Buffer.new( '#Git-diff#' )
    call l:buffer.ignoreMap( 'n', 'q' )
    call l:buffer.map( 'n', 'q', 'quit' )
    call l:buffer.option( 'filetype', 'git-diff' )
    call l:buffer.option( 'spell', '0' )
    call l:buffer.option( 'number', '0' )
    let l:buffer.render = a:render
    call a:parentBuffer.stack.push( l:buffer )
    call a:parentBuffer.stack.active()
endfunction

"//

""
" Method vimdiff() is intended to show differences between two files ( outputs ) in vimdiff mode.
"
" @returns {} - Returns not a value, creates buffers with diffs content.
""

function! s:Class.vimdiff( parentBuffer, source, edited )

    " previous version of file

    let l:source = s:Buffer.new( '#Source file' )
    call l:source.ignoreMap( 'n', 'q' )
    call l:source.map( 'n', 'q', 'quit' )
    call l:source.ignoreAu( 'WinEnter' )
    call a:parentBuffer.stack.push( l:source )
    let l:source._delete = l:source.delete

    function! l:source.delete()
        call self.editFile.delete()
        call self._delete()
    endfunction

    call a:parentBuffer.stack.active()

    silent put = a:source
    0d
    filetype detect
    diffthis

    " current version of file

    let l:edit = s:Buffer.new( '#Edited file' )
    let l:edit.source = l:source
    call l:edit.ignoreMap( 'n', 'q' )
    call l:edit.ignoreAu( 'WinEnter' )
    call l:edit.map( 'n', 'q', 'quit' )
    call l:edit.map( 'n', '<C-y>', 'quit' )

    function! l:edit.quit()
        call self.source.stack.delete()
    endfunction

    call l:edit.vactive( 'after' )
    silent put = a:edited
    0d
    filetype detect
    diffthis

    " to delete both diffs

    let l:source.editFile = l:edit
endfunction

"//

""
" @param { Dictionary } singleton - Instance of class, exists only it.
""

let s:Class.singleton = s:Class.bless()

"//

let g:vim_git#tools#DiffTools# = s:Class
