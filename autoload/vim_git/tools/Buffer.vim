let s:Buffer = vim_lib#sys#Buffer#
let s:Content = vim_lib#sys#Content#.new()
let s:Publisher = vim_lib#sys#Publisher#.new()

"//

""
" The utilitarian class `vim_git#tools#Buffer#` is intended to provide common interface
" for plugin buffers. The class contains all methods of parent class
" `vim_lib#sys#Buffer#` and appends common methods to instances of class.
""

let s:Class = s:Buffer.expand()
let s:Class.buffers = {}
let s:Class.properties = {}
let s:Class.properties.listeners = {}
let s:Class.properties.name = 'vim_git#tools#Buffer#'

"//

""
" The constructor new() rewrites constructor of the parent class `vim_lib#sys#Buffer#`, it creates object view of a buffer.
" Each instance of buffer have some default features.
"
" @param { Number|String } name - Number or the name of destination buffer. If it is not defined, creates new buffer.
" @throws { Exception } If the provided buffer does not exists.
" @returns { Object } - Returns vim_lib#sys#Buffer# destination buffer.
""

function! s:Class.new( ... )
    if exists( 'a:1' )
        let l:bufnr = ( type( a:1 ) == 1 ) ? bufnr( a:1 ) : a:1
        if has_key( self.buffers, l:bufnr )
            return self.buffers[ l:bufnr ]
        endif
    endif

    let l:obj = self.bless()

    let l:obj.number = 0
    if exists( 'a:1' )
        if type( a:1 ) == 0
            if !bufexists( a:1 )
                throw 'IndexOutOfRangeException: Buffer <' . a:1 . '> not found.'
            endif
            let l:obj.number = a:1
        else
            let l:obj.number = bufnr( a:1, 1 )
        endif
    else
        let l:obj.number = bufnr( bufnr( '$' ) + 1, 1 )
    endif

    let l:obj.options = {}
    let l:obj.listenerMap = {}
    let l:obj.listenerAu = {}
    let l:obj.appliedAutocommands = 0

    call l:obj.temp()
    call l:obj.au( 'WinEnter', 'redraw' )
    call l:obj.au( 'InsertEnter', 'autoLeaveInsertMode' )
    call l:obj.au( 'CursorMovedI', 'autoLeaveInsertMode' )
    call l:obj.map( 'n', 'q', 'closeWindow' )
    call l:obj.map( 'n', '?', 'showHelp' )

    let self.buffers[ l:obj.getNum() ] = l:obj
    return l:obj
endfunction

"//

""
" Method autoLeaveInsertMode() sends escape sequence in editor. If it use with some autocommand
" editor will leave insert mode for some action.
"
" @returns {} - Returns not a value, escape from Insert mode.
""

function! s:Class.autoLeaveInsertMode()
    call feedkeys( "\<Esc>", "!" )
endfunction

"//

""
" Method showHelp() is intended to show key mapping for instance of buffer to show help,
" the buffer should contain a List `help`. Also, method can be applied only for temporary
" buffers with method `render`. If uses static buffer, this method should be reimplemented
" or deleted.
"
" @returns {} - Returns not a value, show help for key mapping in current buffer.
""

function! s:Class.showHelp()
    if s:Content.line( 1 ) != self.help[ 0 ]
        let self.pos = s:Content.pos()
        call s:Content.add( 1, self.help )
    else
        call self.active()
        call s:Content.pos( self.pos )
    endif
endfunction

"//

""
" Method closeWindow() closes window of the buffer instance.
"
" @returns {} - Returns not a value, close buffer window.
""

function! s:Class.closeWindow( ... )
    if self.getWinNum() != -1
        call self.stack.clear( 1 )
        call self.stack.current().unload()
        call s:Publisher.fire( 'CloseWindow' )
    endif
endfunction

"//

let g:vim_git#tools#Buffer# = s:Class
