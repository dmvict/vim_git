" Author: DmVict ( dm.vict.kr@gmail.com )
" License: GNU GPL v3 ( http://www.gnu.org/copyleft/gpl.html )

let s:Buffer = g:vim_git#tools#Buffer#
let s:System = vim_lib#sys#System#.new()
let s:Content = vim_lib#sys#Content#.new()
let s:BufferStack = vim_lib#view#BufferStack#
let s:DiffTools = g:vim_git#tools#DiffTools#.new()

"//

let s:screen = s:Buffer.new( '#Git-stash#' )
call s:screen.option( 'cursorline', 1 )
call s:screen.option( 'filetype', 'git-stash' )

"//

""
" Method render() generates data for buffer `StashList`.
"
" @returns { String } - Returns content of buffer `StashList`.
""

function! s:screen.render()
    return '" Stash (Press ? for help) "' . "\n\n" . vim_git#run( 'stash list' )
endfunction

"//

call s:screen.map( 'n', 's',  'show' )
call s:screen.map( 'n', 'p',  'pop' )
call s:screen.map( 'n', 'dd', 'drop' )
call s:screen.map( 'n', 'd',  'diff' )

"//

function! s:getStashName( stash )
    return substitute( a:stash, '\(.*\):', '\1', '' )
endfunction

"//

""
" Method pop() pops stash under cursor from stash list.
"
" @returns {} - Returns not a value, pops stash under cursor.
""

function! s:screen.show()
    call s:Content.pos({ 'c' : 0 })
    call s:System.print( vim_git#stashShow( s:getStashName( expand( '<cWORD>' ) ) ) )
endfunction

"//

""
" Method pop() pops the stash under cursor and applies changes to working tree.
"
" @returns {} - Returns not a value, pops the stash under cursor.
""

function! s:screen.pop()
    call s:Content.pos({ 'c' : 0 })
    call vim_git#stashPop( s:getStashName( expand( '<cWORD>' ) ) )
    call self.redraw()
endfunction

"//

""
" Method drop() drops the stash under cursor.
"
" @returns {} - Returns not a value, drops the stash under cursor.
""

function! s:screen.drop()
    call s:Content.pos({ 'c' : 0 })
    call vim_git#stashDrop( s:getStashName( expand( '<cWORD>' ) ) )
    call self.redraw()
endfunction

"//

""
" Method diff() shows diffs between stash changes and the commit before stash was maiden.
"
" @returns {} - Returns not a value, show diffs.
""

function! s:screen.diff()
    call s:Content.pos({ 'c' : 0 })
    call s:DiffTools.diff( self, "vim_git#stashDiffs('" . s:getStashName( expand( '<cWORD>' ) ) . "')" )
endfunction

"//

""
" The List `help` is intended to show key mapping for `TagList` buffer by using
" method `showHelp` of the instance.
""

let s:screen.help =
\[
\    '" Manual "',
\    '',
\    '" s - show stash diffstats',
\    '" p - pop the stash under cursor',
\    '" dd - drop the stash under cursor',
\    '" d - diffs of stash',
\    ''
\]

"//

let s:bufStack = s:BufferStack.new()
call s:bufStack.push( s:screen )
call s:screen.ignoreMap( 'n', '<C-y>' )
let vim_git#StashList# = s:bufStack

