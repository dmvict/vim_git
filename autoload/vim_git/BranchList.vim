" Date Create: 2015-02-10 22:34:25
" Last Change: see in git
" Author: Artur Sh. Mamedbekov ( Artur-Mamedbekov@yandex.ru )
" Contributor: DmVict ( dm.vict.kr@gmail.com )
" License: GNU GPL v3 ( http://www.gnu.org/copyleft/gpl.html )

let s:Buffer = g:vim_git#tools#Buffer#
let s:Content = vim_lib#sys#Content#.new()
let s:BufferStack = vim_lib#view#BufferStack#
let s:System = vim_lib#sys#System#.new()
let s:DiffTools = g:vim_git#tools#DiffTools#.new()

"//

let s:screen = s:Buffer.new( '#Git-branch#' )
call s:screen.option( 'filetype', 'git-branch' )

function! s:screen.render()
return
    \[
    \'" Branch list (Press ? for help) "',
    \"",
    \'" Local branch',
    \"",
    \vim_git#run( 'branch' ),
    \"",
    \'" Remote branch',
    \"",
    \vim_git#run( 'branch -r' )
    \]
endfunction

"//

call s:screen.map( 'n', '<Enter>', 'checkout' )
call s:screen.map( 'n', 'a',       'new' )
call s:screen.map( 'n', 'dd',      'delete' )
call s:screen.map( 'n', 'r',       'rename' )
call s:screen.map( 'n', 'm',       'merge' )
call s:screen.map( 'n', 'd',       'diff' )
call s:screen.map( 'n', 's',       'show' )
call s:screen.map( 'n', 'o',       'push' )
call s:screen.map( 'n', 'i',       'pull' )

"//

""
" Метод определяет, является ли заданная ветка внешней.
" @param string branch Полное имя ветки.
" @return bool true - если ветка является внешней, иначе - false.
""

function! s:screen.isRemote(branch)
  return stridx(a:branch, '/') != -1
endfunction

"//

""
" Делает заданную ветку текущей.
""

function! s:screen.checkout()
  call vim_git#checkoutBranch(expand('<cWORD>'))
  call self.redraw()
endfunction

"//

""
" Создает новую ветку и делает ее текущей.
""

function! s:screen.new()
  call s:System.echo('Create new branch.', 'ModeMsg')
  let l:branchName = s:System.read('Enter branch name: ')
  if l:branchName != ''
    call vim_git#createBranch(l:branchName)
    call self.redraw()
  endif
endfunction

"//

""
" Удаляет заданную ветку.
""

function! s:screen.delete()
  if s:System.confirm('Realy delete branch "' . expand('<cWORD>') . '"?')
    let l:branchName = expand('<cWORD>')
    if self.isRemote(l:branchName)
      let [l:server, l:branchName] = split(l:branchName, '/')
      call vim_git#deleteRemoteBranch(l:server, l:branchName)
    else
      call vim_git#hardDeleteBranch(l:branchName)
    endif
    call self.redraw()
  endif
endfunction

"//

""
" Переименовывает заданную ветку.
""

function! s:screen.rename()
  call s:System.echo('Rename branch.', 'ModeMsg')
  let l:branchName = s:System.read('Enter branch new name: ')
  if l:branchName != ''
    call vim_git#renameBranch(expand('<cWORD>'), l:branchName)
    call self.redraw()
  endif
endfunction

"//

""
" Сливает заданную ветку с текущей.
""

function! s:screen.merge()
  try
    call vim_git#merge(expand('<cWORD>'))
    call s:System.print('Merge complite', 'MoreMsg')
  catch /^ShellException.*/
  endtry
endfunction

"//

""
" Method diff() shows diff between current branch and selected branch.
"
" @returns {} - Returns not a value, shows diff between current and selected branches.
""

function! s:screen.diff()
    let l:currentBranch = vim_git#run( 'rev-parse --abbrev-ref HEAD' )
    call s:DiffTools.diff( self, "vim_git#run( 'diff " . l:currentBranch[ 0:-2 ] . '..' . expand( '<cWORD>' ) . "' )" )
endfunction

"//

""
" Method show() shows list of files, which was changed in the selected branch.
"
" @returns {} - Returns not a value, shows files changed in branch.
""

function! s:screen.show()
    let l:buffer = s:Buffer.new( '#Git-branch-diff#' )
    let l:buffer.currentBranch = vim_git#currentBranch()
    let l:buffer.diffBranch = expand( '<cWORD>' )
    call l:buffer.option( 'filetype', 'git-commit' )
    call l:buffer.ignoreMap( 'n', 'q' )
    call l:buffer.map( 'n', 'q', 'quit' )
    call l:buffer.map( 'n', 'd', 'diff' )
    call l:buffer.map( 'n', 'D', 'vimdiff' )

    function! l:buffer.render()
        return '" Branch (Press ? for help) "' . "\n\n" . vim_git#run('diff --name-status ' . self.currentBranch . '..' . self.diffBranch)
    endfunction

    function! l:buffer.diff()
        call s:Content.pos({ 'c' : col( '$' ) })
        call s:DiffTools.diff( self, "vim_git#run( 'diff " . self.currentBranch . ".." . self.diffBranch . " -- " . expand( '<cfile>' ) . "')" )
    endfunction

    function! l:buffer.vimdiff()
        call s:Content.pos({ 'c' : col( '$' ) })
        let l:file = expand( '<cfile>' )
        call s:DiffTools.vimdiff( self, readfile( l:file ), vim_git#run( 'cat-file -p ' . self.diffBranch . ':' . l:file ) )
    endfunction

    let l:buffer.help =
    \[
    \    '" Manual "',
    \    '',
    \    '" d - show diff file for branch',
    \    '" D - show diff file for branch with Vim',
    \    ''
    \]

    call self.stack.push( l:buffer )
    call self.stack.active()
endfunction

"//

""
" Выгружает заданную ветку на указанный сервер и делает ее отслеживаемой.
""

function! s:screen.push()
  call s:System.echo('Push branch.', 'ModeMsg')
  let l:servers = split(vim_git#run('remote'), "\n")
  let l:n = 0
  for l:server in l:servers
    call s:System.echo(l:n . '.' . l:server)
    let l:n += 1
  endfor
  let l:n = s:System.read('Select server: ')
  if l:n != ''
    let l:branch = expand('<cWORD>')
    call vim_git#pushBranch(l:servers[l:n], l:branch)
    call self.active()
  endif
endfunction

"//

"
" Загружает и сливает заданную внешнейшнюю ветку с текущей и делает ее отслеживаемой.
""

function! s:screen.pull()
  let l:branchName = expand('<cWORD>')
  if self.isRemote(l:branchName)
    let [l:server, l:branch] = split(l:branchName, '/')
    call vim_git#pullBranch(l:server, l:branch)
  endif
endfunction

"//

""
" The List `help` is intended to show key mapping for `BranchList` buffer by using
" method `showHelp` of the instance.
""

let s:screen.help =
\[
\    '" Manual "',
\    '',
\    '" Local branch "',
\    '"   Enter - checkout branch',
\    '"   a - create new branch',
\    '"   r - rename branch',
\    '"   dd - delete branch',
\    '"   m - merge branch',
\    '"   o - push branch',
\    '"   d - show diff file for branch',
\    '"   s - show branch info',
\    '" Remote branch "',
\    '"   dd - delete branch',
\    '"   m - merge branch',
\    '"   i - pull branch',
\    ''
\]

"//

let s:bufStack = s:BufferStack.new()
call s:bufStack.push( s:screen )
call s:screen.ignoreMap( 'n', '<C-y>' )
let vim_git#BranchList# = s:bufStack

