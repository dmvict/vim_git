" Date Create: 2015-02-10 10:12:11
" Last Change: see in git
" Author: Artur Sh. Mamedbekov ( Artur-Mamedbekov@yandex.ru )
" Contributor: DmVict ( dm.vict.kr@gmail.com )
" License: GNU GPL v3 ( http://www.gnu.org/copyleft/gpl.html )
"

let s:Buffer = g:vim_git#tools#Buffer#
let s:System = vim_lib#sys#System#
let s:Content = vim_lib#sys#Content#.new()
let s:Path = vim_lib#base#Path#
let s:File = vim_lib#base#File#
let s:BufferStack = vim_lib#view#BufferStack#
let s:DiffTools = g:vim_git#tools#DiffTools#.new()

"//

let s:screen = s:Buffer.new( '#Git-log#' )
call s:screen.option( 'filetype', 'git-log' )
call s:screen.option( 'spell', '0' )
call s:screen.option( 'number', '0' )

"//

""
" Method render() generates data for buffer `Log`. The output depends of current value of parameter {-logType-}.
"
" @returns { String } - Returns content of buffer `Log`.
""

function! s:screen.render()
    let l:LogFun = function( 'vim_git#' . g:vim_git#logType . 'Log' )
    let l:limitMsg = ( g:vim_git#logSize == 0 ) ? 'no' : g:vim_git#logSize
    return '" Log [' . l:limitMsg . " limit] (Press ? for help) \"\n\n" . l:LogFun()
endfunction

"// commits

call s:screen.map( 'n', '<Enter>', 'checkout' )
call s:screen.map( 'n', 'u',       'checkoutBranch' )
call s:screen.map( 'n', 'dc',      'diff' )
call s:screen.map( 'n', 'd',       'diffToPrevious' )
call s:screen.map( 'n', 's',       'show' )
call s:screen.map( 'n', 'c',       'toggleType' )
call s:screen.map( 'n', 'f',       'filter' )
call s:screen.map( 'n', 'rs',      'resetSoft' )
call s:screen.map( 'n', 'rh',      'resetHard' )

"// tags

call s:screen.map( 'n', 'ta',       'addAnnotatedTag' )
call s:screen.map( 'n', 'tl',       'addLightweightTag' )
call s:screen.map( 'n', 'td',       'deleteTag' )

"//

""
" Method checkout() makes checkout of the commit in line under cursor. That is makes commit a HEAD commit.
"
" @returns {} - Returns not a value, checkout commit in line under cursor.
""

function! s:screen.checkout()
    call vim_git#checkoutCommit( self.getCommitHashFromLine() )
    call self.redraw()
endfunction

"//

""
" Method checkoutBranch() checkouts pointer to last commit in current branch.
" If HEAD is not on branch then it checkouts to master.
"
" @returns {} - Returns not a value, checkouts to last commit in branch.
""

function s:lastBranchGet()
    let l:branches = split( vim_git#run( 'branch' ) )
    call remove( l:branches, '*' )

    let l:currentHead = s:Content.trim( vim_git#run( 'name-rev --name-only HEAD' ) )
    if index( l:branches, l:currentHead ) != -1
       return l:currentHead
    endif

    let l:length = len( l:branches )
    if l:length == 1
        return l:branches[ 0 ]
    else
        let l:logFile = s:File.absolute( s:Path.join( s:Path.current(), '.git/logs/HEAD' ) ).read()
        let l:i = len( l:logFile ) - 1
        while l:i >= 0
            let l:ref = substitute( l:logFile[ l:i ], '.*checkout.*from \(\S\+\) to.*', '\1', '' )
            if index( l:branches, l:ref ) != -1
                return l:ref
            endif
            let l:i -= 1
        endwhile
    endif
endfunction

function! s:screen.checkoutBranch()
    let l:branch = s:lastBranchGet()
    call vim_git#checkoutBranch( l:branch )
    call self.redraw()
endfunction

"//

""
" Method diff() shows diff between current commit and selected commit.
"
" @returns {} - Returns not a value, shows diff between current and selected commits.
""

function! s:screen.diff()
    call s:DiffTools.diff( self, "vim_git#run('diff " . self.getCommitHashFromLine() . "')" )
endfunction

"//

""
" Method diff() shows diff between selected commit and previous to it.
"
" @returns {} - Returns not a value, shows diff between selected and previous commits.
""

function! s:screen.diffToPrevious()
    let l:hash = self.getCommitHashFromLine()
    call s:DiffTools.diff( self, "vim_git#run('diff " . l:hash . '~1 ' . l:hash . "')" )
endfunction

"//

""
" Method show() shows list of files, which was changed in the selected commit.
"
" @returns {} - Returns not a value, shows files changed in commit.
""

function! s:screen.show()
    let l:buffer = s:Buffer.new( '#Git-commit-diff#' )
    let l:buffer.commit = self.getCommitHashFromLine()
    call l:buffer.option( 'filetype', 'git-commit' )
    call l:buffer.ignoreMap( 'n', 'q' )
    call l:buffer.map( 'n', 'q', 'quit' )
    call l:buffer.map( 'n', 'd', 'diff' )
    call l:buffer.map( 'n', 'D', 'vimdiff' )

    function! l:buffer.render()
        return '" Commit (Press ? for help) "' . "\n\n" . vim_git#run( 'diff --name-status ' . self.commit )
    endfunction

    function! l:buffer.diff()
        call s:Content.pos({ 'c' : col( '$' ) })
        call s:DiffTools.diff( self, "vim_git#run('diff " . self.commit . " -- " . expand('<cfile>') . "')" )
    endfunction

    function! l:buffer.vimdiff()
        call s:Content.pos({ 'c' : col( '$' ) })
        let l:file = expand( '<cfile>' )
        call s:DiffTools.vimdiff( self, readfile( l:file ), vim_git#run( 'cat-file -p ' . self.commit . ':' . l:file ) )
    endfunction

    let l:buffer.help =
    \[
    \    '" Manual "',
    \    '',
    \    '" d - show diff file for commit',
    \    '" D - show diff file for commit with Vim',
    \    ''
    \]

    call self.stack.push( l:buffer )
    call self.stack.active()
endfunction

"//

""
" Method toggleType() changes view for 'git log' output.
"
" @returns {} - Returns not a value, sets view for 'git log' output.
""

function! s:screen.toggleType()
    if g:vim_git#logType == 'classic'
        let g:vim_git#logType = 'graph'
    elseif g:vim_git#logType == 'short'
        let g:vim_git#logType = 'classic'
    else
        let g:vim_git#logType = 'short'
    endif
    call self.redraw()
endfunction

"//

""
" Method filter() is intended to add filters for 'git log' output.
"
" @returns {} - Returns not a value, sets filters for 'git log' output.
""

function! s:screen.filter()
    call s:System.echo( 'Filters:', 'ModeMsg' )
    call s:System.echo( '0. Author [' . g:vim_git#.logAuthor . ']' )
    call s:System.echo( '1. Before [' . g:vim_git#.logBefore . ']' )
    call s:System.echo( '2. After [' . g:vim_git#.logAfter . ']' )
    call s:System.echo( '3. Size [' . g:vim_git#logSize . ']' )
    call s:System.echo( '9. Clear filters' )
    let l:type = s:System.read( 'Select filter type: ' )
    if l:type != ''
        if l:type == 0
            let g:vim_git#.logAuthor = s:System.read( 'Set filter "author": ' )
        elseif l:type == 1
            let g:vim_git#.logBefore = s:System.read( 'Set filter "before": ' )
        elseif l:type == 2
            let g:vim_git#.logAfter = s:System.read( 'Set filter "after": ' )
        elseif l:type == 3
            let g:vim_git#logSize = s:System.read( 'Set filter "size": ' )
        elseif l:type == 9
            let g:vim_git#.logAuthor = ''
            let g:vim_git#.logBefore = ''
            let g:vim_git#.logAfter = ''
            let g:vim_git#logSize = 20
        endif
        call self.redraw()
    endif
endfunction

"//

""
" Method resetSoft() makes soft reset of changes in worktree to commit under cursor.
" Soft reset does not discard changes, it remove commits and applies previous
" commited changes to index.
"
" @returns {} - Returns not a value, makes soft reset of worktree.
" @throws { Exception } - Throws ShellException if command is failed.
""

function! s:screen.resetSoft()
    let l:hash = vim_git#softResetCommit( self.getCommitHashFromLine() )
    call self.redraw()
endfunction

"//

""
" Method resetHard() resets changes in worktree to commit under cursor softly.
" Hard reset discards changes, it remove commits changes and unstaged files.
"
" @returns {} - Returns not a value, makes hard reset of worktree.
" @throws { Exception } - Throws ShellException if command is failed.
""

function! s:screen.resetHard()
    let l:hash = vim_git#hardResetCommit( self.getCommitHashFromLine() )
    call self.redraw()
endfunction

"//

""
" Method getCommitHashFromLine() gets commit hash from line under cursor.
"
" @returns { String } - Return commit hash from line or empty string.
""

function! s:screen.getCommitHashFromLine()
    return matchstr( getline( '.' ), '\zs\x\{7,}' )
endfunction

"// --
"// tags
"// --

""
" Method addLightweightTag() adds lightweight tag to commit in line under cursor.
"
" @returns {} - Returns not a value, adds lightweight tag to commit in line
" under cursor.
" @throws { Exception } - Throws ShellException if command is failed.
""

function! s:screen.addLightweightTag()
    let l:commitHash = self.getCommitHashFromLine()
    if l:commitHash == ''
        call s:System.echo( 'Please, move cursor to line with commit hash.', 'ErrorMsg' )
        return 1
    endif
    let l:tagName = s:System.read( 'Enter tag name : ' )
    if l:tagName != ''
        call vim_git#lightweightTagCommit( l:tagName, l:commitHash )
        call self.redraw()
    endif
endfunction

"//

""
" Method addAnnotatedTag() adds annotated tag to commit in line under cursor.
"
" @returns {} - Returns not a value, adds annotated tag to commit in line
" under cursor.
" @throws { Exception } - Throws ShellException if command is failed.
""

function! s:screen.addAnnotatedTag()
    let l:commitHash = self.getCommitHashFromLine()
    if l:commitHash == ''
        call s:System.echo( 'Please, move cursor to line with commit hash.', 'ErrorMsg' )
        return 1
    endif
    let l:tagName = s:System.read( 'Enter tag name : ' )
    if l:tagName != ''
        let l:tagMessage = s:System.read( 'Enter tag message : ' )
        call vim_git#annotatedTagCommit( l:tagName, l:tagMessage, l:commitHash )
        call self.redraw()
    endif
endfunction

"//

""
" Method deleteTag() adds annotated tag to commit in line under cursor.
"
" @returns {} - Returns not a value, adds annotated tag to commit in line
" under cursor.
" @throws { Exception } - Throws ShellException if command is failed.
""

function! s:screen.deleteTag()
    let l:tag = matchstr( getline( '.' ), 'tag:\s*\zs\(\w\|\S\)*\ze[,)]' )
    if l:tag != ''
        call vim_git#deleteTag( l:tag )
        call self.redraw()
    endif
endfunction

"//

""
" The List `help` is intended to show key mapping for `Status` buffer by using
" method `showHelp` of the instance.
""

let s:screen.help =
\[
\    '" Manual "',
\    '',
\    '" Enter - checkout commit',
\    '" u - checkout to latest switched branch',
\    '" d - show diff between selected and previous commits',
\    '" dc - show diff between current and selected commits',
\    '" s - show commit info',
\    '" c - toogle log type',
\    '" f - set filters',
\    '',
\    '" rs - soft reset to commit under cursor',
\    '" rh - hard reset to commit under cursor',
\    '',
\    '" ta - add annotated tag to commit under cursor',
\    '" tl - add lightweight tag to commit under cursor',
\    '" td - delete tag from commit under cursor',
\    '',
\]

"//

let s:bufStack = s:BufferStack.new()
call s:bufStack.push( s:screen )
call s:screen.ignoreMap( 'n', '<C-y>' )
let vim_git#Log# = s:bufStack
