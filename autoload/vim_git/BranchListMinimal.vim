" Author: DmVict ( dm.vict.kr@gmail.com )
" License: GNU GPL v3 ( http://www.gnu.org/copyleft/gpl.html )

let s:Buffer = g:vim_git#tools#Buffer#
let s:Content = vim_lib#sys#Content#.new()
let s:BufferStack = vim_lib#view#BufferStack#
let s:System = vim_lib#sys#System#.new()
let s:Publisher = vim_lib#sys#Publisher#.new()

"//

let s:screen = s:Buffer.new( '#Git-branch-minimal#' )
call s:screen.au( 'WinEnter', 'listAll' )
call s:screen.option( 'filetype', 'git-branch' )
call s:screen.option( 'spell', '0' )
call s:screen.option( 'number', '0' )

"//

function! s:render()
    return vim_git#run( 'branch --show-current' )
endfunction
let s:screen.render = function( 's:render' )

"//

call s:screen.map( 'n', '<Enter>', 'checkout' )

"//

function! s:screen.listAll()
    let l:bufferNumber = s:Buffer.BufferCurrentNumberGet()
    if winheight( 1 ) > winheight( bufwinnr( l:bufferNumber ) )
    if l:bufferNumber == self.getNum()
        let l:list = vim_git#run( 'branch' )
        let l:items = split( l:list, "\n" )
        let l:length = len( l:items )

        call self.mapUnlistened( 'n', '<LeftMouse>', '<LeftMouse>:call vim_git#BranchListMinimal#.current().checkout()<CR>' )
        let self.render = l:list
        call self.redraw()
        exe 'resize ' . l:length
    endif
    endif
endfunction

"//

function! s:screen.listCurrent()
    let self.render = function( 's:render' )
    call self.redraw()

    let l:bufferNumber = s:Buffer.BufferCurrentNumberGet()
    if winheight( 1 ) > winheight( bufwinnr( l:bufferNumber ) )
        call self.resizeCurrent()
    endif
endfunction

"//

function! s:screen.checkout()
    let l:bufferNumber = s:Buffer.BufferCurrentNumberGet()
    if self.getNum() == l:bufferNumber
        call vim_git#checkoutBranch( expand( '<cWORD>' ) )
        exe 'unmap <buffer> <LeftMouse>'
        call self.listCurrent()
        call s:Publisher.fire( 'UpdateBufferData' )
        call self.mapUnlistened( 'n', '<LeftMouse>', '<LeftMouse>:call vim_git#BranchListMinimal#.current().listAll()<CR>' )
    else
        call self.select()
        call self.listCurrent()
        call s:Buffer.Select( l:bufferNumber )
    endif
endfunction

"//

function! s:screen.resizeCurrent( ... )
    if self.getWinNum() != -1
        let l:previousBuffer = s:Buffer.BufferCurrentNumberGet()
        let l:notSameBuffer = self.getNum() != l:previousBuffer
        if l:notSameBuffer
            call self.select()
        endif
        exe 'resize 1'
        if l:notSameBuffer
            call s:Buffer.Select( l:previousBuffer )
        endif
    endif
endfunction

"//

""
" Method closeWindow() closes window of the buffer instance.
"
" @returns {} - Returns not a value, close buffer window.
""

function! s:screen.closeWindow( ... )
    if self.getWinNum() != -1
        call self.listCurrent()
        call self.stack.clear( 1 )
        call self.stack.current().unload()
        call s:Publisher.fire( 'CloseWindow' )
    endif
endfunction

"//

""
" The List `help` is intended to show key mapping for `BranchList` buffer by using
" method `showHelp` of the instance.
""

let s:screen.help =
\[
\    '" Manual "',
\    '',
\    '" Enter - switch to branch',
\    '" LeftMouse - switch to branch',
\    '',
\]

"//

let s:bufStack = s:BufferStack.new()
call s:bufStack.push( s:screen )
call s:screen.ignoreMap( 'n', '<C-y>' )
autocmd VimResized * call s:Publisher.fire( 'VimResized' )
call s:Publisher.listen( 'VimResized', s:screen.resizeCurrent )
let vim_git#BranchListMinimal# = s:bufStack
