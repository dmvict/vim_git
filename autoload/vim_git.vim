" Date Create: 2015-01-09 13:19:18
" Last Change: see in git
" Author: Artur Sh. Mamedbekov ( Artur-Mamedbekov@yandex.ru )
" Contributor: DmVict ( dm.vict.kr@gmail.com )
" License: GNU GPL v3 ( http://www.gnu.org/copyleft/gpl.html )

let s:Buffer = vim_lib#sys#Buffer#
let s:BufferStack = vim_lib#view#BufferStack#
let s:Sys = vim_lib#sys#System#.new()
let s:Content = vim_lib#sys#Content#.new()

"//

""
" Method runAsync() executes command {-command-} for the buffer asynchronously (uses job).
"
" @param { String } command - Command of the Git to run.
" @param { Dictionary } options - The options for the asynchronous job.
" @returns {} - Returns not a value, appends result of the command to buffer with specified number.
""

function! vim_git#runAsync( command, options )
    call s:Sys.runAsync( g:vim_git#.bin . ' ' . a:command, a:options )
endfunction

"//

""
" Method run() executes Git command and returns buffer with results of it work.
"
" @param { String } command - A Git command.
" @returns { String } - Returns string buffer with results of command work.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#run( command )
    return s:Sys.run( g:vim_git#.bin . ' ' . a:command )
endfunction

"//

""
" Method exe() executes Git command in shell, not in background.
"
" @param { String } command - A Git command.
" @returns {} - Returns not a value, show shell that executes command.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#exe( command )
    call s:Sys.exe( g:vim_git#.bin . ' ' . a:command )
endfunction

"//

""
" Method init() initializes new Git repository.
"
" @returns {} - Returns not a value, initializes repository.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#init()
    call vim_git#exe( 'init' )
endfunction

"// --
"// add
"// --

""
" Method addCurrent() adds current working file to the index.
"
" @returns {} - Returns not a value, adds file {-file-} to the index.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#addCurrent()
    call vim_git#run( 'add ' . expand( '%' ) )
endfunction

"//

""
" Method addFile() adds file {-file-} to the index.
"
" @param { String } file - A file path to add to the index.
" @returns {} - Returns not a value, adds file {-file-} to the index.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#addFile( file )
    call vim_git#run( 'add ' . a:file )
endfunction

"//

""
" Method addAll() adds all unindexed files to the index.
"
" @returns {} - Returns not a value, adds all files to the index.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#addAll()
    call vim_git#run( 'add -A' )
endfunction

"// --
"// stash
"// --

""
" Method stashList() is intended to toggle window with buffer `StashList`.
" First call of method opens the window, the second call turns it off.
"
" @returns {} - Returns not a value, toggle window with a Git StashLilst data.
""

function! vim_git#stashList()
    let l:screen = g:vim_git#StashList#
    if l:screen.current().getWinNum() != -1
        call l:status.clear( 1 )
        call l:status.current().unload()
    else
        call l:screen.vactive( 'right', 40 )
        call s:Content.pos({ 'l' : 3, 'c' : 1 })
    endif
endfunction

"//

""
" Method stashAll() stashes the changes in a dirty working directory away.
"
" @returns {} - Returns not a value, stashes files.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#stashAll()
    call vim_git#run( 'stash' )
endfunction

"//

""
" Method stashFile() stashes the passed file.
"
" @param { String } file - A file path to stash.
" @returns {} - Returns not a value, stashes file.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#stashFile( file )
    call vim_git#run( 'stash -- ' . a:file )
endfunction

"//

""
" Method stashShow() shows the stashed files and info about changes.
"
" @param { String } index - A name of stash to show.
" @returns { String } - Returns info about stashed files.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#stashShow( index )
    return vim_git#run( 'stash show ' . a:index )
endfunction

"//

""
" Method stashDiffs() shows diffs between stash and commit before the stash
" was maiden.
"
" @param { String } index - A name of stash to show diffs.
" @returns { String } - Returns diffs for stash and commit before.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#stashDiffs( index )
    return vim_git#run( 'stash show -p ' . a:index )
endfunction

"//

""
" Method stashPop() pops the stash and applies changes to working tree.
"
" @param { String } index - A name of stash to pop. If no arguments is provided, pops the
" last added stash.
" @returns {} - Returns not a value, pops the stash and applies changes to working tree.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#stashPop( ... )
    let l:stashName = ''
    if( exists( 'a:1' ) )
        let l:stashName = a:1
    endif
    call vim_git#run( 'stash pop ' . l:stashName )
endfunction

"//

""
" Method stashDrop() drops the stash.
"
" @param { String } name - A name of stash to drop.
" @returns {} - Returns not a value, drops the stash.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#stashDrop( ... )
    let l:stashName = ''
    if( exists( 'a:1' ) )
        let l:stashName = a:1
    endif
    call vim_git#run( 'stash drop ' . l:stashName )
endfunction

"//

""
" Method commit() creates new buffer window to write commit message. Commit flashes on
" buffer saving.
"
" @returns {} - Returns not a value, commits files that is added to index with written message.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#commit()
    let l:buf = s:Buffer.new()
    call l:buf.hactive( 'above', 3 )
    exe 'e ' . tempname()
    autocmd BufWritePost <buffer> call vim_lib#sys#System#.new().print( vim_git#run( 'commit --file=' . expand( '%' ) ) )
    autocmd BufWritePost <buffer> exe 'silent bd'
    call l:buf.option( 'filetype', 'git-commit' )
    call l:buf._setOptions()
endfunction

"//

""
" Method fastCommit() writes commit with provided message {-message-}.
"
" @param { String } message - Commit message.
" @returns {} - Returns not a value, commits indexed files with provided message.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#fastCommit( message )
    call vim_git#run( 'commit -m "' . a:message . '"' )
endfunction

"//

""
" Method amendCommit() creates new buffer window to write amend commit message.
"
" @returns {} - Returns not a value, amends last commit with message in buffer.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#amendCommit()
    let l:buf = s:Buffer.new()
    call l:buf.hactive( 'above' )
    exe 'e ' . tempname()
    autocmd BufWritePost <buffer> call vim_lib#sys#System#.new().print( vim_git#run( 'commit --amend --file=' . expand( '%' ) ) )
endfunction

"//

""
" Method commitAll() adds all unindexed files to index and opens new buffer to write commit message.
" Commit flashes on buffer saving.
"
" @returns {} - Returns not a value, commits all files with written message.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#commitAll()
    call vim_git#addAll()
    call vim_git#commit()
endfunction

"//

""
" Method amendCommitAll() performs committing of index and updates last commit.
" For the commit message new buffer is created.
"
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#amendCommitAll()
    call vim_git#addAll()
    call vim_git#amendCommit()
endfunction

"// --
"// reset
"// --

""
" Method resetIndex() resets current indexed files.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#resetIndex()
    call vim_git#run( 'reset' )
endfunction

"//

""
" Method resetFile() resets file {-file-} from the index.
"
" @param { String } file - Filename to be reseted.
"
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#resetFile( file )
    call vim_git#run( 'reset -q -- ' . a:file )
endfunction

"//

""
" Метод исключает текущий файл из индекса.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#resetCurrentFile()
    call vim_git#run( 'reset -- ' . expand( '%' ) )
endfunction

"//

""
" Метод мягко (soft) удаляет историю комитов, начиная с данного.
" @param string commit Комит с которого начнется удаление истории.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#softResetCommit(commit)
    call vim_git#run( 'reset --soft ' . a:commit )
endfunction

"//

""
" Метод удаляет историю комитов, начиная с данного.
" @param string commit Комит с которого начнется удаление истории.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#hardResetCommit(commit)
    call vim_git#run( 'reset --hard ' . a:commit )
endfunction

"//

"// --
"// checkout
"// --

""
" Method undoFile() undo changes in file {-file-}. If the file is untracked
" or untracked ignored, then method will clean the file. If file contains in
" directory that contains only provided file, then method will delete the
" directory also.
"
" @param { String } file - A name of file to reset/delete.
" @returns {} - Returns not a value, reset/delete the provided file from index.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#undoFile( file )
    let l:lines = split( vim_git#run( 'status -s -b -- ' . a:file ), "\n" )
    let l:statusShort = l:lines[ 1 ][ 0:1 ]
    if l:statusShort == '??'
        call vim_git#run( 'clean -dfx -- ' . a:file )
    else
        call vim_git#run( 'checkout HEAD -- ' . a:file )
    endif
endfunction

"//

""
" Method undoChanges() undo all uncommited changes in repository. If the repository
" has untracked or untracked ignored files, then method can clean the files. If
" repository has directories that contains single file, then method will delete the
" directories also.
"
" @param { String } type - An option to clean untracked/ignored files.
" Implements types : 'untracked' and 'untrackedIgnored'.
" @returns {} - Returns not a value, reset changes in repository.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#undoChanges( ... )
    call vim_git#run( 'checkout -f HEAD' )
    if( exists( 'a:1' ) )
        if a:1 == 'untracked'
            call vim_git#run( 'clean -df' )
        elseif a:1 == 'untrackedIgnored'
            call vim_git#run( 'clean -dfx' )
        endif
    endif
endfunction

"//

""
" Метод делает заданный комит текущим.
" @param string commit Целевой комит.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#checkoutCommit( commit )
    call vim_git#run( 'checkout ' . a:commit )
endfunction

"//

""
" Method checkoutHead() returns HEAD pointer to commit in `ORIGIN_HEAD` branch.
"
" @returns {} - Returns not a value, checkout pointer to commit `ORIGIN_HEAD`.
" @throws { Exception } - Throws ShellException if command is failed.
""

function! vim_git#checkoutHead()
    call vim_git#run( 'checkout ORIGIN_HEAD' )
endfunction

"//

""
" Метод делает помеченный комит текущим.
" @param string tag Метка комита.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#checkoutTag( tag )
    call vim_git#run( 'checkout ' . a:tag )
endfunction

""
" Метод делает последний комит заданной ветки текущим.
" @param string branch Целевая ветка.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#checkoutBranch( branch )
    call vim_git#run( 'checkout ' . a:branch )
endfunction

"// --
"// tag
"// --

""
" Method lightweightTag() adds lightweight tag with name {-name-} to latest commit.
"
" @param { String } name - Tag name.
"
" @returns {} - Returns not a value, adds lightweight tag to latest commit.
" @throws { Exception } - Throws ShellException if command is failed.
""

function! vim_git#lightweightTag( name )
    call vim_git#run( 'tag ' . a:name )
endfunction

"//

""
" Method lightweightTagCommit() adds lightweight tag with name { name } to
" defined commit {-commit-}.
"
" @param { String } name - Tag name.
" @param { String } commit - Commit hash.
"
" @returns {} - Returns not a value, adds lightweight tag to commit.
" @throws { Exception } - Throws ShellException if command is failed.
""

function! vim_git#lightweightTagCommit( name, commit )
    call vim_git#run( 'tag ' . a:name . ' ' . a:commit )
endfunction

"//

""
" Method annotatedTag() adds annotated tag with name { name } to
" latest commit.
"
" @param { String } name - Tag name.
" @param { String } message - Tag message.
"
" @returns {} - Returns not a value, adds annotated tag to latest commit.
" @throws { Exception } - Throws ShellException if command is failed.
""

function! vim_git#annotatedTag( name, message )
    call vim_git#run( 'tag -a ' . a:name , ' -m ' . a:commit )
endfunction

"//

""
" Method annotatedTagCommit() adds annotated tag with name { name } to
" defined commit {-commit-}.
"
" @param { String } name - Tag name.
" @param { String } message - Tag message.
" @param { String } commit - Commit hash.
"
" @returns {} - Returns not a value, adds annotated tag to commit.
" @throws { Exception } - Throws ShellException if command is failed.
""

function! vim_git#annotatedTagCommit( name, message, commit )
    call vim_git#run( 'tag -a ' . a:name . ' -m ' . a:message . ' ' . a:commit )
endfunction

"//

""
" Method deleteTag() deletes tag with name { name } from commit.
"
" @param { String } name - Tag name.
"
" @returns {} - Returns not a value, deletes tag from commit.
" @throws { Exception } - Throws ShellException if command is failed.
""

function! vim_git#deleteTag( name )
    call vim_git#run( 'tag -d ' . a:name )
endfunction

"//

""
" Метод показывает информацию о метке.
" @param string name Имя целевой метки.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#showTag( name )
    call s:Sys.print( vim_git#run( 'show -s ' . a:name ) )
endfunction

"//

""
" Method tagList() is intended to toggle window with buffer `TagList`.
" First call of method opens the window, the second call turns it off.
"
" @returns {} - Returns not a value, toggle window with a Git TagLilst data.
""

function! vim_git#tagList()
    let l:screen = g:vim_git#TagList#
    if l:screen.current().getWinNum() != -1
        call l:status.clear( 1 )
        call l:status.current().unload()
    else
        call l:screen.vactive( 'right', 40 )
    endif
endfunction

"//

""
" Method status() is intended to toggle window with buffer `Status`.
" First call of method opens the window, the second call turns it off.
"
" @returns {} - Returns not a value, toggle window with a Git Status data.
""

function! vim_git#status( ... )
    let l:status = g:vim_git#Status#
    if l:status.current().getWinNum() != -1
        call l:status.clear( 1 )
        call l:status.current().unload()
    else
        call l:status.vactive( 'right', 40 )
    endif
endfunction

"// --
"// search
"// --

""
" Method log() is intended to toggle window with buffer `Log`.
" First call of method opens the window, the second call turns it off.
"
" @returns {} - Returns not a value, toggle window with a Git Log data.
""

function! vim_git#search()
    let l:screen = g:vim_git#Search#
    if l:screen.current().getWinNum() != -1
        call l:screen.clear( 1 )
        call l:screen.current().unload()
    else
        let l:query = s:Content.select()
        call l:screen.current().setQuery( l:query )
        call l:screen.hactive( 'below', 10 )
    endif
endfunction

"// --
"// log
"// --

""
" Method log() is intended to toggle window with buffer `Log`.
" First call of method opens the window, the second call turns it off.
"
" @returns {} - Returns not a value, toggle window with a Git Log data.
""

function! vim_git#log()
    let l:screen = g:vim_git#Log#
    if l:screen.current().getWinNum() != -1
        call l:screen.clear( 1 )
        call l:screen.current().unload()
    else
        call l:screen.hactive( 'above' )
    endif
endfunction

"//

""
" Method classicLog() is intended to show output of command `git log` in default format.
"
" @returns { String } - Returns output of the command.
""

function! vim_git#classicLog()
    let l:size = ( g:vim_git#logSize == 0 ) ? '' : '-' . g:vim_git#logSize
    let l:filterAuthor = ( g:vim_git#.logAuthor == '' ) ? '' : '--author="' . g:vim_git#.logAuthor . '"'
    let l:filterAfter = ( g:vim_git#.logAfter == '' ) ? '' : '--after="' . g:vim_git#.logAfter . '"'
    let l:filterBefore = ( g:vim_git#.logBefore == '' ) ? '' : '--before="' . g:vim_git#.logBefore . '"'

    try
        return vim_git#run( 'log ' . l:size . ' ' . l:filterAuthor . ' ' . l:filterAfter . ' ' . l:filterBefore )
    catch /.*/
        return 'Your repository does not have any commits'
    endtry
endfunction

"//

""
" Method graphLog() is intended to show output of command `git log` in graph form.
"
" @returns { String } - Returns output of the command.
""

function! vim_git#graphLog()
    let l:size = ( g:vim_git#logSize == 0 ) ? '' : '-' . g:vim_git#logSize
    let l:filterAuthor = ( g:vim_git#.logAuthor == '' ) ? '' : '--author="' . g:vim_git#.logAuthor . '"'
    let l:filterAfter = ( g:vim_git#.logAfter == '' ) ? '' : '--after="' . g:vim_git#.logAfter . '"'
    let l:filterBefore = ( g:vim_git#.logBefore == '' ) ? '' : '--before="' . g:vim_git#.logBefore . '"'

    try
        return vim_git#run( 'log ' . l:size . ' ' . l:filterAuthor . ' ' . l:filterAfter . ' ' . l:filterBefore . ' --graph --pretty="format:%h [%ar by %an] - %s %d"' )
    catch /.*/
        return 'Your repository does not have any commits'
    endtry
endfunction

"//

""
" Method shortLog() is intended to show output of command `git log` in short and formatted form.
"
" @returns { String } - Returns output of the command.
""

function! vim_git#shortLog()
    let l:size = ( g:vim_git#logSize == 0 ) ? '' : '-' . g:vim_git#logSize
    let l:filterAuthor = ( g:vim_git#.logAuthor == '' ) ? '' : '--author="' . g:vim_git#.logAuthor . '"'
    let l:filterAfter = ( g:vim_git#.logAfter == '' ) ? '' : '--after="' . g:vim_git#.logAfter . '"'
    let l:filterBefore = ( g:vim_git#.logBefore == '' ) ? '' : '--before="' . g:vim_git#.logBefore . '"'

    try
        return vim_git#run( 'log ' . l:size . ' ' . l:filterAuthor . ' ' . l:filterAfter . ' ' . l:filterBefore . ' --pretty="format: %h %s %d"' )
    catch /.*/
        return 'Your repository does not have any commits'
    endtry
endfunction

"//

""
" Method logSearchDiffs() is intended to find commits with diffs which include string {-src-}.
"
" @param { String } src - Source string to search.
"
" @returns { String } - Returns output of the command.
""

function! vim_git#logSearchDiffs( src )
    let l:query = escape( a:src, "\"()[]" )
    return vim_git#run( 'log -G "' . l:query . '" --pretty="format: %ai %an %n %h %s %d"' )
endfunction

"// --
"// branch
"// --

""
" Метод определяет текущую ветку.
" @return string Имя текущей ветки.
""

function! vim_git#currentBranch()
    return vim_git#run( 'rev-parse --abbrev-ref HEAD' )[0 : -2]
endfunction

"//

""
" Метод создает новую ветку.
" @param string name Имя создаваемой ветки.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#createBranch( name )
    call vim_git#run( 'branch ' . a:name )
    call vim_git#checkoutBranch( a:name )
endfunction

"//

""
" Метод удаляет ветку.
" @param string name Имя удаляемой ветки.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#softDeleteBranch( name )
    call vim_git#run( 'branch -d ' . a:name )
endfunction

"//

""
" Метод удаляет ветку, даже если она не была слита с другой веткой.
" @param string name Имя удаляемой ветки.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#hardDeleteBranch( name )
    call vim_git#run( 'branch -D ' . a:name )
endfunction

"//

""
" Метод удаляет ветку на удаленном сервере.
" @param string alias Псевдоним сервера.
" @param string name Имя удаляемой ветки.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#deleteRemoteBranch( alias, name )
    call vim_git#exe( 'push ' . a:alias . ' :' . a:name )
endfunction

"//

""
" Метод переименовывает заданную ветку.
" @param string name Имя целевой ветки.
" @param string newname Новое имя ветки.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#renameBranch( name, newname )
    call vim_git#run( 'branch -M ' . a:name . ' ' . a:newname )
endfunction

"//

""
" Метод сливает указанную ветку с текущей.
" @param string name Имя сливаемой ветки.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#merge( branch )
    call vim_git#run( 'merge ' . a:branch )
endfunction

"//

""
" Метод отображает список веток репозитория в новом окне.
""

function! vim_git#branchList()
    let l:screen = g:vim_git#BranchList#
    if l:screen.current().getWinNum() != -1
        call l:screen.clear( 1 )
        call l:screen.current().unload()
    else
        call l:screen.vactive( 'right', 40 )
    endif
endfunction

"//

" remote
""
" Метод создаен пресводинм сервера.
" @param string name Псевдоним.
" @param string url URL адрес сервера.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#createRemote( name, url )
    call vim_git#run( 'remote add ' . a:name . ' ' . a:url )
endfunction

"//

""
" Метод удаляет псевдоним сервера.
" @param string name Удаляемый псевдоним.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#deleteRemote( name )
    call vim_git#run( 'remote rm ' . a:name )
endfunction

"//

""
" Метод изменяет псевдоним сервера.
" @param string name Псевдоним.
" @param string newname Новый псевдоним.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#renameRemote( name, newname )
    call vim_git#run( 'remote rename ' . a:name . ' ' . a:newname )
endfunction

"//

""
" Метод загружает все изменения из указанного сервера.
" @param string name Псевдоним целевого сервера.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#fetch( name )
    call vim_git#exe( 'fetch ' . a:name )
endfunction

"//

""
" Метод загружает все изменения из указанного сервера и сливает их с текущей веткой.
" @param string name Псевдоним целевого сервера.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#pull( name )
    call vim_git#exe( 'pull ' . a:name )
endfunction

"//

""
" Метод загружает все изменения из сервера и сливает их с текущей веткой. Текущая ветка должна являться отслеживаемой.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#pullCurrent()
    call vim_git#exe( 'pull' )
endfunction

"//

""
" Method pullAll fetches all changes from all remote servers and merge it with current working branch.
"
" @returns {} - Returns not a value, fetch data and merge with branch.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#pullAll()
    call vim_git#run( 'pull --all' )
endfunction

"//

""
" Метод загружает ветку из указанного сервера в текущую.
" @param string server Псевдоним целевого сервера.
" @param string branch Имя ветки.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#pullBranch( server, branch )
    call vim_git#exe( 'pull ' . a:server . ' ' . a:branch )
endfunction

"//

""
" Метод выгружает все изменения на указанный сервер.
" @param string name Псевдоним целевого сервера.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#push( name )
    call vim_git#exe( 'push ' . a:name )
endfunction

"//

""
" Method pushCurrent() pushes local unpushed commits to remote server.
" If commit local commits history has conflicts with remote commits history,
" and {-force-} is 1, this method overwrite remote commits history by local.
"
" @param { Boolean } force - A flag that enable overwriting of commits history.
" @returns {} - Returns not a value, pushes unpushed commits.
" @throws { Exception } - Throws ShellException if command failed.
""

function! vim_git#pushCurrent( ... )
    let l:force = ''
    if( exists( 'a:1' ) && a:1 == 1 )
        let l:force = ' --force'
    endif
    call vim_git#exe( 'push' . l:force )
endfunction

"//

""
" Метод выгружает заданную ветку на сервер.
" @param string server Псевдоним целевого сервера.
" @param string branch Имя ветки.
" @throws ShellException Выбрасывается в случае ошибки при выполнении команды.
""

function! vim_git#pushBranch( server, branch )
    call vim_git#exe( 'push --set-upstream ' . a:server . ' ' . a:branch . ':' . a:branch )
endfunction

"//

""
" Метод формирует список псевдонимов серверов.
""

function! vim_git#remoteList()
    let l:screen = g:vim_git#RemoteList#
    if l:screen.getWinNum() != -1
        call l:screen.unload()
    else
        call l:screen.vactive( 'right', 40 )
    endif
endfunction

"//

function! vim_git#help()
    h vim_git
endfunction

"//

""
" Method compositeWindow() is intended to toggle composite window with buffers of the plugin
" declared in map {-compositeConfig-}.
" First call of method opens window, the second call turns it off.
"
" @returns {} - Returns not a value, toggle window with a buffers.
""

function! vim_git#compositeWindow( ... )
    let l:bufferMap =
    \{
    \  'status' : g:vim_git#Status#,
    \  'log' : g:vim_git#Log#,
    \  'branchList' : g:vim_git#BranchList#,
    \  'branchListMinimal' : g:vim_git#BranchListMinimal#,
    \  'tagList' : g:vim_git#TagList#,
    \  'remoteList' : g:vim_git#RemoteList#,
    \}
    let l:window = g:vim_git#compositeConfig
    let l:keys = keys( l:window )
    let l:length = len( l:keys )

    let l:i = 0
    while l:i < l:length
        if l:bufferMap[ l:window[ l:keys[ l:i ] ].name ].current().getWinNum() != -1
            while l:i < l:length
                try
                    call l:bufferMap[ l:window[ l:keys[ l:i ] ].name ].clear( 1 )
                    call l:bufferMap[ l:window[ l:keys[ l:i ] ].name ].current().unload().
                catch /.*/
                endtry
                let l:i += 1
            endwhile
            return
        endif
        let l:i += 1
    endwhile

    call l:bufferMap[ l:window.buffer1.name ][ l:window.buffer1.align . 'active' ]( l:window.buffer1.mode, l:window.buffer1.size )
    call l:bufferMap[ l:window.buffer1.name ].current().option( 'winfixwidth', 1 )
    let l:direction = l:window.buffer1.align == 'v' ? 'hactive' : 'vactive'
    let l:mode = l:direction == 'vactive' ? 'after' : 'below'

    let l:i = 1
    while l:i < l:length
        call l:bufferMap[ l:window[ l:keys[ l:i ] ].name ][ l:direction ]( l:mode, l:window[ l:keys[ l:i ] ].size . '%' )
        let l:i += 1
    endwhile
    call l:bufferMap[ l:window.buffer1.name ].current().select()
endfunction

