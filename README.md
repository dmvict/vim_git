# vim_git

### Plugin for convenient using of Git

Fork of [Bashka/vim_git](https://github.com/Bashka/vim_git) on GitHub.

### About

The author of the first version of the plugin created a plugin system and a base library for it. A time ago I've used it without any changes. But some features was disgusted for me. So, this fork shows another vision of using Git tools inside Vim.

#### Features

- **Functions for binding** - All functionality provided by autoloaded functions. It can be binded.
- **Separate window with buffers** - Plugin implements 5 buffers: Status, Log, BranchList, RemoteList, TagList.
- **Composite Git window** - It is possible to use single command `vim_git#compositeWindow` to toggle window with user declared buffers.
- **Keybindings for buffer** - Each buffer implements Git command. It is possible to perform Git command inside buffers using predefined keybindings.
- **Two form of diffs** - The plugin provides functionality to show diffs in two form - Git diffs and Vim diffs.

### Installation

Plugin **vim_git** depends on one base library [**vim_lib**](https://gitlab.com/dmvict/vim_lib).

If you are using some plugin manager install both plugins.

#### Installation with **Pathnogen**

To install plugin run next commands:

```
git clone https://gitlab.com/dmvict/vim_lib ~/.vim/bundle/vim_lib
git clone https://gitlab.com/dmvict/vim_prj ~/.vim/bundle/vim_git
```

#### Installation using **vim_lib** plugin

The base library **vim_lib** inside has a plugin manager. It works like **Vundle**.

The manual of using plugin manager in library is given in the documentation. It's a simplified example.

Add next code at the start of your `.vimrc` file:

```
filetype off
set rtp=~/.vim/bundle/vim_lib
call vim_lib#sys#Autoload#init('~/.vim', 'bundle')
Plugin 'vim_lib'
Plugin 'vim_git'

" other plugins

filetype indent plugin on
```

The variable `rtp` defines path to library. It initializes command `Plugin`. So, line `Plugin [name]` connects plugin with specified name in directory that is defined in routine `init` - in example uses path `~/.vim' + 'bundle => ~/.vim/bundle/`. At the same time, plugin can use any path.

### Configuration

The plugin has a few options that can be declared in your `.vimrc` file.

Type of output in Log buffer:

```
let g:vim_git#logType = 'short'
```

It can be:
- **short** - only hash and commit message;
- **classic** - classic log;
- **graph** - log in graph form.

The number of the records in Log buffer:

```
let g:vim_git#logSize = 25
```

The buffer Log will show only 25 records. If this value set to 0, the number of records will be unlimited.

Settings for composite window:

```
let g:vim_git#compositeConfig =
\{
\   'buffer1' : { 'name' : 'status', 'align' : 'v', 'mode' : 'R', 'size' : 22 },
\   'buffer2' : { 'name' : 'log', 'size' : 50 }
\}
```

This dictionary defines set of Git buffers in common window. Option `buffer1` defines main buffer, it has a little more fields than other.
For option `buffer1`:
- name - name of buffer. It can be: status, log, tagList, branchList, remoteList;
- align - align of the window: v -vertically, g - horizontal ( author of original plugin system use this shortcut );
- mode - position of buffer in the screen: B, b, T, t, R, r, L, l;
- size - size in percent of the screen dimension.

### P.S.

I'm not a Vim plugins developer. It's work for fun and improving daily usage of my favorite editor. If you have the best solution for some routines you can create PR or send me a message.
Thanks!
