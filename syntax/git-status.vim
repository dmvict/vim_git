runtime syntax/diff.vim
set syntax=git-base

syntax match BufferName             /\" Status.* \"/
syntax match gitStatusBranch        /Branch::/
syntax match gitStatusBranch        /Remote branch::/
syntax match gitStatusBranchName    /\%3l\%11c.*/
syntax match gitStatusBranchName    /\%4l\%18c.*/
syntax match gitStatusStashes       /Repository has/
syntax match gitStatusStashes       /stash(es)/
syntax match gitStatusStashesLength /\zs\d\+\ze stash(es)/

syntax match gitStatusAdded     /\zsadded.*/
syntax match gitStatusModified  /\zsmodified.*/
syntax match gitStatusDeleted   /\zsdeleted.*/
syntax match gitStatusRenamed   /\zsrenamed.*/
syntax match gitStatusUndracked /\zsuntracked.*/

syntax match Comment /Added to index :/
syntax match Comment /Unstaged :/
syntax match Comment /Untracked :/
syntax match Comment /Unmerged :/

highlight link BufferName                 Identifier
highlight link gitStatusBranch            Function
highlight link gitStatusBranchName        Special
highlight link gitStatusStashes           Function
highlight link gitStatusStashesLength     Special
highlight link gitStatusAdded             String
highlight link gitStatusModified          Type
highlight link gitStatusDeleted           Character
highlight link gitStatusRenamed           Constant
highlight link gitStatusUndracked         Character
