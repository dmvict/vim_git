runtime syntax/diff.vim

highlight diffAdded         cterm=none ctermfg=Green ctermbg=Black gui=none guifg=Green guibg=Black
highlight diffRemoved       cterm=none ctermfg=Red   ctermbg=Black gui=none guifg=Red guibg=Black
