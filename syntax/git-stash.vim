set syntax=git-base

syntax match BufferName           /\" StashList.* \"/
syntax match gitStashName         /^\S\+:/
syntax match gitStashCommitName   / \zs.*/

highlight link BufferName           Identifier
highlight link gitStashName         Title
highlight link gitStashCommitName   Type
