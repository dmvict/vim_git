set syntax=git-base

syntax match BufferName           /\" Log.* \"/
syntax match gitLogCommit         /^commit \x\{40}/
syntax match gitLogCommit         /\*[ |]*\zs\x\{7,}\ze/
syntax match gitLogAuthor         /^Author: \zs\w\+\ze/
syntax match gitLogAuthor         /by \zs\w\+\ze\]/
syntax match gitLogShortHash      /^\s\x\{7,}\s/
syntax match gitLogCommitMessage  /^\s\{2,}.*/
syntax match gitLogCommitMessage  /\(\s\x\{7,}\s\)\@<=.*/

highlight link BufferName           Identifier
highlight link gitLogShortHash      Title
highlight link gitLogCommitMessage  Tag
highlight link gitLogCommit         Statement
highlight link gitLogAuthor         Comment
