" Date Create: 2015-01-09 16:02:43
" Last Change: see in git
" Author: Artur Sh. Mamedbekov ( Artur-Mamedbekov@yandex.ru )
" Contributor: DmVict ( dm.vict.kr@gmail.com )
" License: GNU GPL v3 ( http://www.gnu.org/copyleft/gpl.html )

let s:Plugin = vim_lib#sys#Plugin#
let s:Publisher = vim_lib#sys#Publisher#.new()

"//

let s:p = s:Plugin.new( 'vim_git', '1' )

"//

""
" Options
""

""
" @param { String } bin - The name of command Git.
""

let s:p.bin = 'git'

"//

""
" @param { Number } logSize - Defines quantity of records in the log output.
" The value `0` - unlimited number of records.
""

if( !exists( 'g:vim_git#logSize' ) )
    let g:vim_git#logSize = 30
endif

"//

""
" @param { Boolean } statusWithStash - Enables logging of stashes in status output.
""

if( !exists( 'g:vim_git#statusWithStash' ) )
    let g:vim_git#statusWithStash = 0
endif

"//

""
" @param { String } logType - Defines type of the log output.
" The option can be:
" graph - graph in short form;
" classic - default log;
" short - short form ( only hash and message ).
""

if( !exists( 'g:vim_git#logType' ) )
    let g:vim_git#logType = 'classic'
endif

"//

""
" Filters for Git Log command.
" @param { String } logAuthor - Filter for log, sets author of commits.
" @param { String } logAfter - Filter for log, sets date from which commits was
" added.
" @param { String } logBefore - Filter for log, sets date until which commits
" was added.
""

let s:p.logAuthor = ''
let s:p.logAfter = ''
let s:p.logBefore = ''

"//

""
" Method run() provides initializing of plugin. It adds autocommands for buffers in composite window.
""

"//

function! s:p.run()
    if !exists( 'g:vim_git#compositeConfig' )
        return
    endif

    let l:bufferMap =
    \{
    \  'status' : g:vim_git#Status#,
    \  'log' : g:vim_git#Log#,
    \  'branchList' : g:vim_git#BranchList#,
    \  'branchListMinimal' : g:vim_git#BranchListMinimal#,
    \  'tagList' : g:vim_git#TagList#,
    \  'remoteList' : g:vim_git#RemoteList#,
    \}

    for l:buffer in keys( g:vim_git#compositeConfig )
        call s:Publisher.listen( 'CloseWindow', l:bufferMap[ g:vim_git#compositeConfig[ l:buffer ].name ].current().closeWindow )
    endfor
    call s:Publisher.listen( 'UpdateBufferData', 'vim_git#compositeWindow' )
    call s:Publisher.listen( 'UpdateBufferData', 'vim_git#compositeWindow' )
endfunction

"//

" Меню
""
" Отобразить статус репозитория.
""
call s:p.menu('Status', 'status', '1')
""
" Добавить текущий файл в индекс.
""
call s:p.menu('Index.Add', 'addCurrent', '2.1')
""
" Добавить все измененные файлы в индекс.
""
call s:p.menu('Index.Add_all', 'addAll', '2.2')
""
" Удалить все файлы из индекса.
""
call s:p.menu('Index.Reset', 'resetIndex', '2.3')
""
" Удалить текущий файл из индекса.
""
call s:p.menu('Index.Reset_file', 'resetCurrentFile', '2.4')
""
" Отменить все изменения.
""
call s:p.menu('Index.Undo', 'undoChanges', '2.5')
""
" Отобразить историю комитов.
""
call s:p.menu('Log', 'log', '3')
""
" Вернуться к предыдущему комиту.
""
call s:p.menu('Commit.Checkout_branch', 'checkoutBranch', '4.1')
""
" Создать комит.
""
call s:p.menu('Commit.Commit', 'commit', '4.2')
""
" Добавить все инзмененные файлы в индекс и создать комит.
""
call s:p.menu('Commit.Commit_all', 'commitAll', '4.3')
""
" Создать замещающий комит.
""
call s:p.menu('Commit.Amend_commit', 'amendCommit', '4.4')
""
" Добавить все инзмененные файлы в индекс и создать замещающий комит.
""
call s:p.menu('Commit.Amend_commit_all', 'amendCommitAll', '4.5')
""
" Отобразить список веток.
""
call s:p.menu('Branch.List', 'branchList', '5')
""
" Отобразить список псевдонимов серверов.
""
call s:p.menu('Remote.List', 'remoteList', '6.1')
""
" Выгрузить текущую ветку на сервер.
""
call s:p.menu('Remote.Push', 'pushCurrent', '6.2')
""
" Загрузить изменения с сервера в текущую ветку.
""
call s:p.menu('Remote.Pull', 'pullCurrent', '6.3')
""
" Отобразить список меток.
""
call s:p.menu('Tag.List', 'tagList', '7')
""
" Инициализировать новый репозиторий.
""
call s:p.menu('Init', 'init', '8')
""
" Отобразить документацию плагина.
""
call s:p.menu('Help', 'help', '9')

"//

call s:p.reg()
